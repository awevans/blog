---
title: Idee uitkiezen
date: 2017-09-11
---
De dag begonnen met een uitleg over je team, er werd verteld dat je regels emoest vaststellen. Daarna gingen we in de projectgroepjes samen zitten aan een tafel. We zijn begonnen met het samenstellen van de regels. Ihsan hebben we tot leider gemaakt, samen hebben we ook studio eisen verzonnen en die hebben de teamleiders samen vastgesteld onder elkaar. 

We zijn begonnen met een onderzoek, vastgesteld wat we wilden onderzoeken. Bijvoorbeeld wat onze doelgroep is en wat voor “game” we willen gaan maken. Dit hebben we verdeeld onder ons groepje en zo hebben we allemaal een verschillend stukje onderzoek gedaan. Mijn onderzoek ging over verschillende games die er zijn. Ik heb vooral gekeken naar verschillende interactieve games en zo voorbeelden verzonnen voor ons eigen spel. 

We moesten een poster maken waar we onze kwaliteiten, achtergrond, ambitie en regels op hadden gezet. Op de poster kwam ook onze team naam: “**Mozaique**”.
