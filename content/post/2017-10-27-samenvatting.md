---
title: Samenvatting Blog
date: 2017-10-27
---
Het kwartaal zit erop en het is tijd om een samenvatting en reflectie te maken over het blog.

Het blog was een interessante ervaring. Ik snapte er in het begin helemaal niks van, dus heb ik het een hele tijd bijgehouden op een andere manier.
Uiteindelijk heb ik Bob gevraagd wat er mis ging en waarom mijn blog niet werkte. Nadat hij een hoofdletter naar een kleine letter veranderde, deed alles het en kon ik mijn blog eindelijk gaan bewerken.

Als ik terugkijk naar wat we allemaal hebben gedaan, hebben we eigenlijk best veel in korte tijd gemaakt. Ons eerste spel ging heel goed, we hadden allemaal leuke ideeën ervoor.
Het was een origineel concept, visueel zag het er vrolijk uit. En toen kwam de "Kill your darling". Dit raakte het team en volgens mij de hele klas erg hard. Iedereen was zo gefocust op het spel waar ze mee bezig waren inclusief ons team.
Mijn eerste gedachte was: Waarom? De weken daarna ging het allemaal wat moeizaam in het groepje en iedereen was een beetje de motivatie kwijt geraakt. Dit leidde ertoe dat onse tweede spel niet even leuk of goed was als het eerste.
Althans, dat is wat mij opviel. Na een tijdje kwamen we wel weer een beetje bij en begonnen we weer met het brainstormen over het nieuwe spel.

Later ben ik erachter gekomen wat nou de reden was voor de "kill your darling. Later als we aan het werk zijn voor ons zelf of bij een design studio, zullen er klanten komen met een project. Als je dan aan dit project gaat werken, moet je uiteindelijk 
laten zien wat je hebt gemaakt aan de opdrachtgever. Dan komt er een keer dat de opdrachtgever zegt dat hij/zij het helemaal niks vind. Dat betekent dat je helemaal opnieuwe moet beginnen. En dan kan het zo zijn dat jij het zelf helemaal top vind.
Maar uiteindelijk betaalt de opdrachtgever voor het project en is die de baas. En dit kan moeilijk zijn, want als designer heb jij de ervaring en de skills en weet je veel beter dan de opdrachtgever wat er goed is voor de gebruiker. 

De "kill your darling" was dus lastig maar wel goed en belangrijk.

