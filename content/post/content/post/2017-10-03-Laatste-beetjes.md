---
title: Laatste beetjes
date: 2017-10-03
---
We hadden besloten om vandaag naar school te komen om het concept af te maken voor de presentatie. We hadden nog best een aantal dingen te doen, het meeste hiervan hebben we dus ook kunnen afmaken. We hebben het spel getest met wat studenten van een andere klas. Hieruit hebben we een analyse kunnen maken die we in de presentatie hebben kunnen uitleggen. Verder hebben we aan de presentatie gewerkt en we hadden besloten dat Sico en Remco het gingen presenteren. Mijn taak was om de feedback op te schrijven. 