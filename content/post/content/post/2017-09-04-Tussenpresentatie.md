---
title: Tussenpresentatie
date: 2017-09-13
---

Vandaag hadden we een soort tussenpresentatie van het paper prototype. Met een deel van de klas hebben we gekeken naar de verschillende prototypes. We hebben feedback gekregen op onze prototype zodat we dit kunnen verbeteren voor de echte app. 

Na de presentatie hebben we 2 klasgenoten buiten ons groepje gevraagd of ze het paper prototype wilde testen. Samen met Ashley, Jiska en de rest van ons groepje hebben we de game getest en hebben we wat meer conclusies kunnen vastleggen.
