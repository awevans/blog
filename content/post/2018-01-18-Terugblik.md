---
title: Terugblik
date: 2018-01-18
---
Het is weer snel gegaan, kwartaal 2 zit erop. Tijdens het kwartaal voelt het alsof het soms te lang duurt, maar als je dan terugkijkt dan is het toch echt weer snel gegaan. Ik heb deze periode weer veel kunnen leren. Goede peerfeedback gehad, veel feedback was wat ik in het vorige kwartaal ook heb gekregen. Dit betekent dat ik er nog steeds aan moet werken, maar wel dat ik de kans heb om het nog beter op te pakken de volgende keer. 
Heb moeite gehad met de validaties, na een tijdje snapte ik pas wat ik er echt aan had. Hopelijk kan ik er de volgende periode meer bijwonen en gaat het dan ook wat duidelijker. 
Met het team lekker samengewerkt, gezellig gehad en vooral gelachen. We hadden niet een super grote expositie, maar toch ben ik wel trots op wat we hebben gemaakt en uiteindelijk kunnen laten zien. 
Volgende kwartaal ook wat meer aan mijn blog werken :)